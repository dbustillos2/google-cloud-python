#Python module used for google cloud folder deployments

import pulumi 
import yaml
import google_folders 

with open('config.yaml') as config_file:
    try:
        config = yaml.safe_load(config_file)

    except yaml.YAMLError as exc:
            print(exc)

parent = 'organizations/856354845949'

google_folders.folderStructure(config, parent)
