#Python functions used for google cloud folder deployments

import pulumi
import pulumi_gcp as google


def folderStructure(config, parent):
    for folderRoot in config:
        #build folder with folders name
        orgLevelFolder = folderBuilder(folderRoot, parent)
        for nested in config[folderRoot]:
            folderBuilder(nested, orgLevelFolder.id)
            #build folder with nested folder

def folderBuilder(name, parent):
    return google.organizations.Folder(name, 
    display_name=name, 
    parent=parent)



